var keys = document.querySelectorAll('#kalkulator span');
var operators = ['+', '-', 'x', '÷'];
var decimalAdded = false;

for(var i = 0; i < keys.length; i++) {
	keys[i].onclick = function(e) {
		var input = document.querySelector('.screen');
		var inputVal = input.innerHTML;
		var btnVal = this.innerHTML;

		if(btnVal == 'C') {
			input.innerHTML = '';
			decimalAdded = false;
		}
			
		else if(btnVal == '=') {
			var equation = inputVal;
			var lastChar = equation[equation.length - 1];
			// console.log('awal',equation);	
			equation = equation.replace(/x/g, '*').replace(/:/g, '/');
			// console.log('akhir',equation);	
			if(equation)
				input.innerHTML = eval(equation);
				decimalAdded = false;
		}
			

			
		else if(operators.indexOf(btnVal) > -1) {
			var lastChar = inputVal[inputVal.length - 1];
			
			if(inputVal != '' && operators.indexOf(lastChar) == -1) 
				input.innerHTML += btnVal;
						
			else if(inputVal == '' && btnVal == '-') 
				input.innerHTML += btnVal;
				
			if(operators.indexOf(lastChar) > -1 && inputVal.length > 1) {

				input.innerHTML = inputVal.replace(/.$/, btnVal);
			}
					
			decimalAdded =false;
		}
			
		else {
			input.innerHTML += btnVal;
		}
				
		// prevent page jumps
		e.preventDefault();
	} 
}